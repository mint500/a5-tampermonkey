// ==UserScript==
// @name         academyFIVE::MoveNavbarIcons
// @namespace    tvog/academyfive
// @version      2024-05-13
// @description  Verschiebt die Icons zur Steuerung des Baummenüs auf die linke Seite.
// @author       Tobias Vogler
// @match        https://a5.meine-hochschule.de/*
// @match        https://a5.deine-hochschule.de/*
// @grant        none
// @icon         https://www.academyfive.com/typo3conf/ext/sitepackage/Resources/Public/build/assets/images/favicon-academyfive.ico
// ==/UserScript==

(function() {
    'use strict';

    // neue position der buttons
    const left = "330px";

    var intv = setInterval(function() {

        var controlLayerElements = document.querySelectorAll('[id^="controlLayer"]');

        if(controlLayerElements.length < 1){
            // kein element gefunden. weiterhin warten
            return false;
        }

        // element gefunden. setze intervall zurück und ändere die ausrichtung der Buttons
        clearInterval(intv);

        for (var i = 0, len = controlLayerElements.length; i < len; i++){
            console.log('Moving menu controls to ' + left + '...');
            controlLayerElements[i].style.left = left;
        }


    }, 100);


})();