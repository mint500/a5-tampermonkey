// ==UserScript==
// @name         academyFIVE::DisplayActiveSiteName
// @namespace    tvog/academyfive
// @version      2024-05-13
// @description  Zeigt den aktuell gewählten Standort in der Menüleiste am oberen Bildschirmrand an.
// @author       Tobias Vogler
// @match        https://a5.meine-hochschule.de/*
// @match        https://a5.deine-hochschule.de/*
// @grant        none
// @icon         https://www.academyfive.com/typo3conf/ext/sitepackage/Resources/Public/build/assets/images/favicon-academyfive.ico
// ==/UserScript==

(function() {
    'use strict';

    var siteSelectPicker;
    var sites;
    var activeSite;
    var dropdown;
    var dropdownChildren;

    // suche das dropdown-menü für die standortauswahl
    siteSelectPicker = document.getElementById("siteSelectPicker");

    if(siteSelectPicker) {
        // suche alle kinderelemente der standortauswahl. dies sind die einzelnen standorte plus "Alle Standorte"
        sites = siteSelectPicker.childNodes;

        for (var i = 0, len = sites.length; i < len; i++){
            if(sites[i].selected == true) {
                // dies ist der aktuell gewählte standort. diesen merken.
                activeSite = sites[i].text;
                continue;
            }
        }

        // finde das element mit der klasse "siteDropDown".
        // davon sollte es nur eines geben. nimm also nur das erste element aus der sammlung
        dropdown = document.getElementsByClassName("siteDropDown")[0];

        if(dropdown) {
            // dropdown gefunden. es besteht aus mehreren "einzelteilen" und wir benötigen das kindelement mit dem a-tag (link)
            // hole also alle kindelemente
            dropdownChildren = dropdown.childNodes;

            // und durchlaufe diese
            for (var j = 0, len2 = dropdownChildren.length; j < len2; j++){
                if(dropdownChildren[j].tagName == "A") {
                    // wenn der link gefunden ist, ändere den linktext auf den namen des aktuell ausgewählten standortes
                    dropdownChildren[j].innerHTML = '<i class="fa-regular fa-house"></i> \[' + activeSite + '\] <span class="caret"></span>';
                    continue;
                }
            }
        }
    }
    
})();